const gulp = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const gulpSass = require("gulp-sass");
const nodeSass = require("node-sass");
const scss = gulpSass(nodeSass);
const postcss = require('gulp-postcss');
const pxtorem = require('postcss-pxtorem');
const sourcemaps = require('gulp-sourcemaps');
const dgbl = require('del-gulpsass-blank-lines');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const babel = require('gulp-babel');
const gcmq = require("gulp-group-css-media-queries"); // 중복되는 mediaquery 구문 merge
const cleanCSS = require('gulp-clean-css'); // css minify -> gcmq 사용 시 sass outputStyle이 적용이 안되므로 css compressed를 위해 추가

const path = {
	// mobile을 위한 path 정의
	dest: './resource/',
	src: './mobile/',

	// pc을 위한 path 정의
	// dest: './resourcePc/',
	// src: './pc/',
}
const manageEnvironment = (environment) => {
	environment.addFilter('tabIndent', (str, numOfIndents, firstLine) => {
		str = str.replace(/^(?=.)/gm, new Array(numOfIndents + 1).join('\t'));
		if (!firstLine) {
			str = str.replace(/^\s+/, "");
		}
		return str;
	});
};
// gulp.task('guide', () => {
// 	return gulp.src('./guide/src/*.html')
// 		.pipe(nunjucksRender({
// 			envOptions: {
// 				autoescape: false
// 			},
// 			manageEnv: manageEnvironment,
// 			path: ['./guide/list','./guide/src']
// 		}))
// 		.pipe(gulp.dest('./guide/'))
// 		.pipe(browserSync.reload({ stream: true }));
// });
gulp.task('html', () => {
	return gulp.src(path.src + 'html/**/*.html')
		.pipe(nunjucksRender({
			envOptions: {
				autoescape: false
			},
			manageEnv: manageEnvironment,
			path: [path.src]
		}))
		.pipe(gulp.dest(path.dest + 'html'))
		.pipe(browserSync.reload({ stream: true }));
});

const cssName = 'globals.css' // scss 최종 컴파일 파일명
const scssOptions = {
	outputStyle: 'compact', /* nested, expanded, compact, compressed */
	indentType: 'tab',
	indentWidth: 1,
	sourceComments: false
}
const plugins = [
	pxtorem({
		rootValue: '10',
		propList: ['*'],
		unitPrecision: 2,
		mediaQueries: true
	})
];
gulp.task('css', () => {
	return gulp
		.src(path.src + 'css/**/*.css')
		// .pipe(gcmq())
		// .pipe(cleanCSS())
		.pipe(dgbl())
		// .pipe(postcss(plugins)) //px -> rem 단위로 변경시 주석 제거
		.pipe(gulp.dest(path.dest + 'css'))
		.pipe(browserSync.reload({ stream: true }));
});
gulp.task('scss', () => {
	return gulp
		.src(path.src + 'scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(scss(scssOptions).on('error', scss.logError))
		// .pipe(gcmq())
		// .pipe(cleanCSS())
		.pipe(dgbl())
		.pipe(rename(cssName))
		// .pipe(postcss(plugins)) //px -> rem 단위로 변경시 주석 제거
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(path.dest + 'css'))
		.pipe(browserSync.reload({ stream: true }));
});

gulp.task('js', () => {
	return gulp
		.src(path.src + 'js/**/*.js')
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(gulp.dest(path.dest + 'js'))
		.pipe(browserSync.reload({ stream: true }));
})
gulp.task('browserSync', () => {
	return browserSync.init({
		port: 3333,
		server: {
			baseDir: './',

			// mobile을 위한 indext path
			index: './resource/html/index2.html'

			// pc를 위한 indext path
			// index: './resourcePc/html/index2.html' 
		}
	})
});
gulp.task('watch', () => {
	// gulp.watch('./guide/list/*.html', gulp.series('guide'));
	gulp.watch(path.src + '**/*.html', gulp.series('html'));
	gulp.watch(path.src + '**/*.scss', gulp.series('scss'));
	gulp.watch(path.src + '**/*.css', gulp.series('css'));
	gulp.watch(path.src + '**/*.js', gulp.series('js'));
});

// gulp.task('default', gulp.parallel('guide','html','scss','js','watch','browserSync'));
// gulp.task('build', gulp.parallel('guide','html','scss','js'));

gulp.task('default', gulp.parallel('html','css','scss','js','watch','browserSync'));
gulp.task('build', gulp.parallel('html','css','scss','js'));